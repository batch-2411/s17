/*
FUNCTIONS
	Functions in javascript are lines/block of codes that tell ur device/application to perform a certain task when called/invoked

	Functions are mostly created to create complicated tasks to run several lines of code in succession

	Functions are also used to prevent repeating lines/blocks of codes that perform the same task/function
*/

// Function Declaration
	// (function statement) defines a function with the specidifed parameters

/*
	Syntax:
		function functionName() {
			codeblock(statements)
		}

	function keywork - used to define a javascript functions
	functionName - the function name. FUnction are named to be able to use later in the code
	function block ({}) - statements which comprise the body of the function. This where code to be executed
*/

console.log("My name is John Outside");

function printName() {
	console.log("My name is John!");
}

// Function Invocation
/*
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
*/

// Invoke/call the function that we declared
printName();

// declaredFunction(); 

// Function Declaration vs Function Expressions
	// Function Declaration
		// A function can br created through function declaration by using the function keyword and adding the function name
		// Declared function are NOT executed immediately. They are "saved for later used" and will be executed later when they are invoked(called).

	// Hoisting
	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from the declaredFunction()");
	}

	declaredFunction();

	// Function Expressions
		// A function can be stored in a variable. This is called function expression

		// A function expression is an anonymous function assigned to the variableFunction

		// Anonymous function - a function without a name

	// variableFunction(); Uncaught ReferenceError: Cannot access 'variableFunction' before initialization
	let variableFunction = function() {
		console.log("Hello from the variableFunction");
	}

	variableFunction();

	let functionExpression = function funcName() {
		console.log("Hello from the other side!");
	}

	functionExpression();
	// funcName();

	declaredFunction = function() {
		console.log("Updated declaredFunction");
	}

	declaredFunction();

	functionExpression = function() {
		console.log("Updated functionExpression");
	} 

	functionExpression();


	// We cannot re-assigned a function that was declared using const
	const constantFunc = function() {
		console.log("Hello constantFunc");
	}

	constantFunc();

// Function Scoping
/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope
		1. local/block scope
		2. global scope
		3. function scope
*/

	{
		let localVar = "I am a local variable.";
	}

	let globalVar = "I am a global variable.";

	function showNames() {
		// Function scoped variables
		/*
			The variables, functionVar, functionConst, functionLet are function scoped variables. They can only be accessed inside of the function they were declared in.
		*/
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// Nested Function
		// You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable "name" as they are within the same code block/scope

	function myNewFunction() {
		let name = "Maria";

		function nestedFunction() {
			let nestedName = "Jose";
			console.log(name);
		}

		// console.log(nestedName);
		nestedFunction();
	}

	myNewFunction();

	// Function and Global Scope Variables

		// Global Variable
	let globalName = "Erven Joshua";

	function myNewFunction2() {
		let nameInside = "Jenno"

		console.log(globalName);
	}

	myNewFunction2();

	// console.log(nameInside);

	// USING ALERT
	/*
		Syntax:
			alert(message)
	*/

		// alert

		function showSampleAlert() {
			alert("Hello User!");

		}

	// showSampleAlert();
	console.log("Hello pows!");

	// USING PROMPT()
	/*
		Syntax:
			prompt("<DialogInString>");
	*/

		// let samplePrompt = prompt("Enter your name:");

		// console.log("Hello, " + samplePrompt)
		console.log(typeof samplePrompt)


		function printWelcomeMessage() {
			let firstName = prompt("Enter your First Name:");
			let lastName = prompt("Enter your Last Name");

			console.log("Hello " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		}

		// printWelcomeMessage();

// Function Naming Conventions
	// Name your function in small caps. Follows camelCase when naming variables and functions

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();

	// Function name should be defenitive of the task it will perform. It usually contains a verb

	function getCourse() {
		let courses = ["Science 101", "Math 101", "English 101"];

		console.log(courses);
	}

	getCourse();

	// Avoid generic names to avoid consfusion within your code
	function get() {
		let name = "Jamie";
		console.log(name);
	}

	get();

	// Avoid pointless and inappropriate function names
	function foo() {
		console.log(25 % 5);
	}

	foo();